/*
 * Copyright 2003 Pavel Machek <pavel@ucw.cz>,
 *
  This is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

#include <qpe/qpeapplication.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qvariant.h>
#include <qprogressbar.h>
#include <qdialog.h>
#include <qtimer.h>
#include <qmessagebox.h>
#include <qlineedit.h>
#include <qmultilineedit.h>

#include "gnokii.h"
#include "mainwin.ui.h"
#include "smssend.ui.h"

static struct gn_statemachine state;

void mainwin::refresh()
{
	float rflevel = -1, batterylevel = -1;
	gn_rf_unit rfunit = GN_RF_Arbitrary;
	gn_battery_unit battunit = GN_BU_Arbitrary;
	gn_data data;
	char buf[10240];

	gn_data_clear(&data);
	data.rf_unit = &rfunit;
	data.rf_level = &rflevel;
	data.battery_unit = &battunit;
	data.battery_level = &batterylevel;

	if (gn_sm_functions(GN_OP_GetRFLevel, &data, &state) == GN_ERR_NONE) {
		sprintf(buf, "Signal: %d", (int) rflevel);
		signal_label->setText(buf);
		signal_bar->setProgress((int) rflevel);
	}

	if (gn_sm_functions(GN_OP_GetBatteryLevel, &data, &state) == GN_ERR_NONE) {
		battery_label->setText("Battery:");
		battery_bar->setProgress((int) batterylevel);
	}
}

void mainwin::sms_clicked()
{
	smssend *d = new smssend();
	d->show();
}

static gn_error readtext(gn_sms_user_data *udata, int input_len, char *text)
{
	char message_buffer[255 * GN_SMS_MAX_LENGTH];
	int chars_read;

	strcpy(message_buffer, text);
	/* Get message text from stdin. */
	chars_read = strlen(message_buffer);

	if (chars_read == 0) {
		fprintf(stderr, "Couldn't read from stdin!\n");
		return GN_ERR_INTERNALERROR;
	} else if (chars_read > input_len || chars_read > sizeof(udata->u.text) - 1) {
		fprintf(stderr, "Input too long! (%d, maximum is %d)\n", chars_read, input_len);
		return GN_ERR_INTERNALERROR;
	}

	/* Null terminate. */
	message_buffer[chars_read] = 0x00;
	if (udata->type != GN_SMS_DATA_iMelody && chars_read > 0 && message_buffer[chars_read - 1] == '\n') 
		message_buffer[--chars_read] = 0x00;
	strncpy((char *) udata->u.text, message_buffer, chars_read);
	udata->u.text[chars_read] = 0;
	udata->length = chars_read;

	return GN_ERR_NONE;
}

void smssend::send()
{
	const char *number, *text;
	gn_sms sms;
	gn_error error;
	gn_data data;

	gn_data_clear(&data);

	number = number_line->text();
	text = text_multiline->text();

	/* The memory is zeroed here */
	gn_sms_default_submit(&sms);

	memset(&sms.remote.number, 0, sizeof(sms.remote.number));
	strncpy(sms.remote.number, number, sizeof(sms.remote.number) - 1);
	if (sms.remote.number[0] == '+') 
		sms.remote.type = GN_GSM_NUMBER_International;
	else 
		sms.remote.type = GN_GSM_NUMBER_Unknown;

	sms.delivery_report = true;

	if (!sms.smsc.number[0]) {
		data.message_center = (gn_sms_message_center *) calloc(1, sizeof(gn_sms_message_center));
		data.message_center->id = 1;
		if (gn_sm_functions(GN_OP_GetSMSCenter, &data, &state) == GN_ERR_NONE) {
			strcpy(sms.smsc.number, data.message_center->smsc.number);
			sms.smsc.type = data.message_center->smsc.type;
		}
		free(data.message_center);
	}

	if (!sms.smsc.type) sms.smsc.type = GN_GSM_NUMBER_Unknown;

	error = readtext(&sms.user_data[0], 160, (char *) text);
	if (error != GN_ERR_NONE) return;
	if (sms.user_data[0].length < 1) {
		fprintf(stderr, "Empty message. Quitting.\n");
	}
	sms.user_data[0].type = GN_SMS_DATA_Text;
	if (!gn_char_def_alphabet(sms.user_data[0].u.text))
		sms.dcs.u.general.alphabet = GN_SMS_DCS_UCS2;
	sms.user_data[1].type = GN_SMS_DATA_None;

	data.sms = &sms;

	/* Send the message. */
	error = gn_sms_send(&data, &state);

	if (error == GN_ERR_NONE)
		QMessageBox::critical ( 0, number, "Send succeeded!" );
	else
		QMessageBox::critical ( 0, "SMS Send failed", gn_error_print(error) );

	close();
}

int main(int argc, char **argv)
{
	static char *bindir;

	QPEApplication a( argc, argv );

	if (gn_cfg_read(&bindir) >= 0)
		if (gn_cfg_phone_load("", &state))
			if (gn_gsm_initialise(&state) == GN_ERR_NONE)
				{}

	mainwin *d = new mainwin();
	a.setMainWidget( d ); d->show(); d->refresh(); return a.exec();

}
    
