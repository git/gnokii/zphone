/****************************************************************************
** Form interface generated from reading ui file 'smssend.ui'
**
** Created: Mon Jul 14 23:16:14 2003
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef SMSSEND_H
#define SMSSEND_H

#include <qvariant.h>
#include <qdialog.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QLabel;
class QLineEdit;
class QMultiLineEdit;
class QPushButton;

class smssend : public QDialog
{ 
    Q_OBJECT

public:
    smssend( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~smssend();

    QLabel* TextLabel3;
    QLineEdit* number_line;
    QMultiLineEdit* text_multiline;
    QPushButton* addressbook_button;
    QPushButton* send_button;
    QPushButton* options_button;
    QLabel* length_label;

public slots:
    virtual void send();
};

#endif // SMSSEND_H
