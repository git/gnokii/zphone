/****************************************************************************

  Copyright 2003 Pavel Machek <pavel@ucw.cz>

  This is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

****************************************************************************/
#include "mainwin.ui.h"

#include <qlabel.h>
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/* 
 *  Constructs a mainwin which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
mainwin::mainwin( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "mainwin" );
    resize( 241, 150 ); 
    setCaption( tr( "ZGnokii" ) );

    battery_bar = new QProgressBar( this, "battery_bar" );
    battery_bar->setGeometry( QRect( 100, 70, 130, 26 ) ); 

    refresh_button = new QPushButton( this, "refresh_button" );
    refresh_button->setGeometry( QRect( 130, 110, 101, 21 ) ); 
    refresh_button->setText( tr( "Refresh" ) );

    sms_button = new QPushButton( this, "sms_button" );
    sms_button->setGeometry( QRect( 10, 110, 90, 21 ) ); 
    sms_button->setText( tr( "SMS" ) );

    signal_bar = new QProgressBar( this, "signal_bar" );
    signal_bar->setGeometry( QRect( 100, 40, 130, 26 ) ); 

    signal_label = new QLabel( this, "signal_label" );
    signal_label->setGeometry( QRect( 10, 40, 80, 21 ) ); 
    signal_label->setText( tr( "Signal: ERR" ) );

    battery_label = new QLabel( this, "battery_label" );
    battery_label->setGeometry( QRect( 10, 70, 81, 21 ) ); 
    battery_label->setText( tr( "Battery: ERR" ) );

    operator_label = new QLabel( this, "operator_label" );
    operator_label->setGeometry( QRect( 10, 10, 220, 21 ) ); 
    operator_label->setText( tr( "ERROR" ) );

    // signals and slots connections
    connect( refresh_button, SIGNAL( clicked() ), this, SLOT( refresh() ) );
    connect( sms_button, SIGNAL( clicked() ), this, SLOT( sms_clicked() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
mainwin::~mainwin()
{
    // no need to delete child widgets, Qt does it all for us
}
