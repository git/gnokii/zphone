include Makefile.inc

all: main

clean:
	rm -f *.o mainwin.moc.cpp smssend.moc.cpp

main.o: main.cpp
	$(GPLUSPLUS) $(CFLAGS) $(LIBS) $(INCS) -o $@ $<

mainwin.moc.cpp: mainwin.ui.h
	$(MOC) mainwin.ui.h > mainwin.moc.cpp

smssend.moc.cpp: smssend.ui.h
	$(MOC) smssend.ui.h > smssend.moc.cpp

mainwin.ui.o: mainwin.ui.cpp
	$(GPLUSPLUS) $(CFLAGS) $(LIBS) $(INCS) -o $@ $<

mainwin.moc.o: mainwin.moc.cpp
	$(GPLUSPLUS) $(CFLAGS) $(LIBS) $(INCS) -o $@ $<

smssend.ui.o: smssend.ui.cpp
	$(GPLUSPLUS) $(CFLAGS) $(LIBS) $(INCS) -o $@ $<

smssend.moc.o: smssend.moc.cpp
	$(GPLUSPLUS) $(CFLAGS) $(LIBS) $(INCS) -o $@ $<

main: main.o mainwin.ui.o mainwin.moc.o smssend.ui.o smssend.moc.o  
	$(GPLUSPLUS) -o main main.o mainwin.ui.o mainwin.moc.o smssend.ui.o smssend.moc.o  -L$(QPEDIR)/lib -lqpe -lqte -lgnokii
