/****************************************************************************
** Form interface generated from reading ui file 'mainwin.ui'
**
** Created: Mon Jul 14 22:35:38 2003
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef MAINWIN_H
#define MAINWIN_H

#include <qvariant.h>
#include <qdialog.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QLabel;
class QProgressBar;
class QPushButton;

class mainwin : public QDialog
{ 
    Q_OBJECT

public:
    mainwin( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~mainwin();

    QProgressBar* battery_bar;
    QPushButton* refresh_button;
    QPushButton* sms_button;
    QProgressBar* signal_bar;
    QLabel* signal_label;
    QLabel* battery_label;
    QLabel* operator_label;

public slots:
    virtual void refresh();
    virtual void sms_clicked();
};

#endif // MAINWIN_H
