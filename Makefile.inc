OPIEDIR=/opt/Embedix/tools/arm-linux
QTDIR=/opt/Qtopia
QPEDIR=/opt/Qtopia/sharp

GPLUSPLUS=g++
CC=gcc
MOC=moc
UIC=uic

INCS = -I$(OPIEDIR)/include -I$(QTDIR)/include -I/usr/include/qte2 -I../gnokii/include -I$(QPEDIR)/include

LIBS = -L$(QTDIR)/lib

CFLAGS = -c -pipe -DQWS -fno-exceptions -fno-rtti -Wall -W -O2 -fno-default-inline -DNO_DEBUG
