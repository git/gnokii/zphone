/*
 * Copyright 2003 Pavel Machek <pavel@ucw.cz>,
 *
  This is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

#include <stdio.h>
#include "gnokii.h"

static struct gn_statemachine state;

void refresh()
{
	float rflevel = -1, batterylevel = -1;
	gn_rf_unit rfunit = GN_RF_Arbitrary;
	gn_battery_unit battunit = GN_BU_Arbitrary;
	gn_data data;
	char buf[10240];

	gn_data_clear(&data);
	data.rf_unit = &rfunit;
	data.rf_level = &rflevel;
	data.battery_unit = &battunit;
	data.battery_level = &batterylevel;

	if (gn_sm_functions(GN_OP_GetRFLevel, &data, &state) != GN_ERR_NONE)
		return;

	if (gn_sm_functions(GN_OP_GetBatteryLevel, &data, &state) != GN_ERR_NONE)
		return;

	{
		FILE *f = fopen("/tmp/anyapplet.active.list", "w+");
#define PICPATH "../../../../../../../home/pavel/sf/zphone/gsmapplet/pics/"
		fprintf(stderr, "Data okay\n");
		fprintf(f, PICPATH "batt%d\n", (int) batterylevel/8);
		fprintf(f, PICPATH "signal%d\n", (int) rflevel/2);
		fclose(f);
	}
}

int main(int argc, char **argv)
{
	static char *bindir;

	if (gn_cfg_read(&bindir) >= 0)
		if (gn_cfg_phone_load("", &state))
			if (gn_gsm_initialise(&state) == GN_ERR_NONE)
				{}

	while(1) {
		refresh();
		sleep(1);
	}
}
    
