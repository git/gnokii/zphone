/****************************************************************************

  Copyright 2003 Pavel Machek <pavel@ucw.cz>

  This is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

****************************************************************************/
#include "smssend.ui.h"

#include <qlabel.h>
#include <qlineedit.h>
#include <qmultilineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/* 
 *  Constructs a smssend which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
smssend::smssend( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "smssend" );
    resize( 247, 274 ); 
    setCaption( tr( "Send SMS message" ) );

    TextLabel3 = new QLabel( this, "TextLabel3" );
    TextLabel3->setGeometry( QRect( 10, 10, 30, 21 ) ); 
    TextLabel3->setText( tr( "To:" ) );

    number_line = new QLineEdit( this, "number_line" );
    number_line->setGeometry( QRect( 40, 10, 150, 26 ) ); 

    text_multiline = new QMultiLineEdit( this, "text_multiline" );
    text_multiline->setGeometry( QRect( 10, 50, 220, 151 ) ); 
    text_multiline->setMaxLength(150);

    addressbook_button = new QPushButton( this, "addressbook_button" );
    addressbook_button->setGeometry( QRect( 201, 10, 30, 31 ) ); 
    addressbook_button->setText( tr( "..." ) );

    send_button = new QPushButton( this, "send_button" );
    send_button->setGeometry( QRect( 171, 220, 60, 31 ) ); 
    send_button->setText( tr( "Send" ) );

    options_button = new QPushButton( this, "options_button" );
    options_button->setGeometry( QRect( 70, 220, 90, 31 ) ); 
    options_button->setText( tr( "Options..." ) );

    length_label = new QLabel( this, "length_label" );
    length_label->setGeometry( QRect( 10, 210, 51, 21 ) ); 
    length_label->setText( tr( "0/150" ) );

    // signals and slots connections
    connect( send_button, SIGNAL( clicked() ), this, SLOT( send() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
smssend::~smssend()
{
    // no need to delete child widgets, Qt does it all for us
}

