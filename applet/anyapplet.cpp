/**********************************************************************
** Copyright (C) 2002 David Woodhouse <dwmw2@infradead.org>
**                    Max Reiss <harlekin@handhelds.org> [trivial stuff]
**                    Robert Griebl <sandman@handhelds.org>
**               2003 Pavel Machek <pavel@ucw.cz>
** 
** Based on IrdaApplet from opie project.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
**********************************************************************/

#include <qcopchannel_qws.h>

#include <qpe/qpeapplication.h>
#include <qpe/resource.h>
#include <qpe/ir.h>
#include <qpe/qcopenvelope_qws.h>
#include <qpe/sound.h>

#include <qpainter.h>
#include <qfile.h>
#include <qtimer.h>
#include <qtextstream.h>
#include <qpopupmenu.h>

#include <unistd.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "anyapplet.h"

//===========================================================================

#define VARPATH "/tmp/anyapplet."

AnyApplet::AnyApplet ( QWidget *parent, const char *name )
		: QWidget ( parent, name )
{
	setFixedHeight ( 18 );
	setFixedWidth ( 14 );

	m_popup = 0;

        QCopChannel* chan = new QCopChannel("QPE/AnyApplet", this );
        connect(chan, SIGNAL(received(const QCString&, const QByteArray&) ),
                this, SLOT(slotMessage(const QCString&, const QByteArray&) ));
}

void AnyApplet::show()
{
	QWidget::show ();
	startTimer(2000);
}

AnyApplet::~AnyApplet()
{
}

void AnyApplet::popup(QString message, QString icon)
{
	if (!m_popup)
		m_popup = new QPopupMenu(this);

	m_popup->clear();
	if (icon.isEmpty())
		m_popup->insertItem(message, 0);
	else
		m_popup->insertItem(QIconSet( Resource::loadPixmap(icon)), message, 0);

	QPoint p = mapToGlobal(QPoint(0, 0));
	QSize s = m_popup->sizeHint();
	m_popup-> popup( QPoint( p.x() + ( width() / 2 ) - ( s.width() / 2 ),
				 p.y() - s.height()));

	QTimer::singleShot(2000, this, SLOT(popupTimeout()));
}

void AnyApplet::popupTimeout()
{
	m_popup->hide();
}

void AnyApplet::mousePressEvent ( QMouseEvent * )
{
	QPopupMenu *menu = new QPopupMenu ( this );
	QString cmd;

	/* Refresh active state */
	timerEvent ( 0 );

	QFile menufile(VARPATH "menu");
	int menupos = 0;
	if (menufile.open(IO_ReadOnly)) {
		QString text;
		QStringList list =  QStringList::split("\n", QTextStream(&menufile).read());

		for (QStringList::Iterator lit = list.begin(); lit != list.end(); ++lit ) {
			const QString &line = *lit;
			if (line == "---")
				menu->insertSeparator();
			else
				menu->insertItem(line, menupos++);
		}
	}
	QPoint p = mapToGlobal(QPoint(0, 0));
	QSize s = menu->sizeHint();
	p = QPoint(p.x() + (width() / 2) - (s.width() / 2), p.y() - s.height());

	int menuitem = menu->exec(p);
	char buf[10240];
	sprintf(buf, VARPATH "sh %d", menuitem);
	if (system( (const char *) buf )) {
		/* Strange, did exec fail? */
	}

	/* Refresh again to pick up messages */
	timerEvent ( 0 );
		
	delete menu;
}

void AnyApplet::timerEvent(QTimerEvent *)
{
	QFile message(VARPATH "msg");
	if (message.open(IO_ReadOnly)) {
		QString msg = QTextStream(&message).read();
		popup(msg);
		unlink(VARPATH "msg");
	}

	QFile *listfile = new QFile(VARPATH "active.list");
	if (listfile->open(IO_ReadOnly)) {
		unlink(VARPATH "active.list");
	} else {
		delete listfile;
		listfile = new QFile(VARPATH "fallback.list");
		if (!listfile->open(IO_ReadOnly)) {
			popup("Fallback list not present");
			return;
		}
	}

	pixmap_list = QStringList::split("\n", QTextStream(listfile).read());
	if (1)
		update ();
}

void AnyApplet::paintEvent(QPaintEvent *)
{
	QPainter p(this);

	for (QStringList::Iterator lit = pixmap_list.begin(); lit != pixmap_list.end(); ++lit ) {
		const QString &line = *lit;
		QPixmap pixmap =  Resource::loadPixmap(line);
		p.drawPixmap(0, 1, pixmap);
	}
}

void AnyApplet::slotMessage(const QCString&, const QByteArray&)
{
}
