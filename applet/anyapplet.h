/**********************************************************************
** Copyright (C) 2002 L.J. Potter ljp@llornkcor.com,
**                    Robert Griebl sandman@handhelds.org
**               2003 Pavel Machek pavel@ucw.cz
**  All rights reserved.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
**********************************************************************/

#ifndef __OPIE_IRDA_APPLET_H__
#define __OPIE_IRDA_APPLET_H__

#include <qwidget.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qmap.h>

class AnyApplet : public QWidget
{
	Q_OBJECT

public:
	AnyApplet( QWidget *parent = 0, const char *name = 0 );
	~AnyApplet();

	virtual void show ( );

protected:
	virtual void timerEvent ( QTimerEvent * );
	virtual void mousePressEvent ( QMouseEvent * );
	virtual void paintEvent ( QPaintEvent* );

private slots:
	void popupTimeout ( );
        void slotMessage( const QCString& , const QByteArray& );

private:
	void popup( QString message, QString icon = QString::null );
	void showDiscovered();

private:
	QPopupMenu *m_popup;
	QStringList pixmap_list;

	QMap <QString, QString> m_devices;

	bool m_wasOn; // if IrDa was enabled
	bool m_wasDiscover;
};


#endif // __OPIE_IRDA_APPLET_H__
